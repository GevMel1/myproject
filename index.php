<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

    <?php
    $name = 'Gevorg';
    $age = 18;
    echo "<span>Hello, my name is</span> $name <br>";
    echo "<span>I'm </span> $age <br>";
    foreach (str_split($name) as $char) {
        echo $char . ' ';
    }
    ?>

</body>
</html>